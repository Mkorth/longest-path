**Done using typescript so all the meat and potatos is in graph.ts**

Requires Typescript to compile `npm i -g typescript`

To compile: `tsc`

To run: `node ./graph.js`

**Example output:**
```
Paths from 1
1
1>2
1>2>4
1>2>4>3
1>3
1>3>2
1>3>2>4
1>3>4
Longest: 3
[ [ 1, 2, 4, 3 ], [ 1, 3, 2, 4 ] ]

Paths from 2
2
2>4
2>4>3
Longest: 2
[ [ 2, 4, 3 ] ]

Paths from 3
3
3>2
3>2>4
3>4
Longest: 2
[ [ 3, 2, 4 ] ]

Paths from 4
4
4>3
4>3>2
Longest: 2
[ [ 4, 3, 2 ] ]
```