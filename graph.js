"use strict";
class Graph {
    constructor(numVerticies, edges) {
        this._verticies = 0;
        this._edges = [];
        this._paths = [];
        this._longest = 0;
        this._longestTrips = [];
        this._verticies = numVerticies;
        this._edges = edges;
        // for every vertex, keep an array of destinations
        this._edges.forEach((edge) => {
            if (!this._paths[edge.from])
                this._paths[edge.from] = [];
            this._paths[edge.from].push(edge.to);
        });
    }
    walk(visited, current, trip, count) {
        // because everything in JS is a pointer...
        let newVisited = visited.map((val) => val);
        newVisited[current] = true;
        let newTrip = trip.map((val) => val);
        // increment our counters
        newTrip.push(current);
        count += 1;
        this._longestTrips.push(newTrip);
        // filter so that the only available paths are unvisited for this run
        const paths = !!this._paths[current] ? this._paths[current].filter((destination) => {
            return newVisited[destination] !== true;
        }) : [];
        // draw our trip
        console.log(newTrip.join('>'));
        // subtract 1 from count since we counted our starting point
        if (count - 1 > this._longest)
            this._longest = count - 1;
        // continue every possible trip until there's no where else to go
        paths.forEach((destination) => {
            this.walk(newVisited, destination, newTrip, count);
        });
    }
    find(from) {
        this._longest = 0;
        this._longestTrips = [];
        let visited = new Array(this._verticies);
        this.walk(visited, from, [], 0);
        // since the recursive walk function is blocking, this works for reporting it.
        console.log('Longest: ' + this._longest);
        this._longestTrips = this._longestTrips.filter((trip) => {
            return trip.length === this._longest + 1;
        });
        console.log(this._longestTrips);
    }
    get verticies() {
        return this._verticies;
    }
}
const edges = [
    {
        from: 1,
        to: 2
    },
    {
        from: 1,
        to: 3
    },
    {
        from: 3,
        to: 2
    },
    {
        from: 2,
        to: 4
    },
    {
        from: 3,
        to: 4
    },
    {
        from: 4,
        to: 3
    }
];
const graph = new Graph(4, edges);
for (let i = 0; i < graph.verticies; i++) {
    console.log('\nPaths from ' + (i + 1));
    graph.find(i + 1);
}
